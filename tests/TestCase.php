<?php

namespace LiliPay\Tests;

use LiliPay\Models\CreditCard;
use LiliPay\Models\ExpirationDate;
use LiliPay\Models\Plan;
use LiliPay\Tests\Models\Sale;
use LiliPay\Tests\Models\User;
use LiliPay\Tests\Models\Address;
use LiliPay\Tests\Models\Item;
use LiliPay\Interfaces\SaleInterface;
use LiliPay\Gateways\Pagarme\Models\PaymentMethod;
use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\CreditCardInterface;
use LiliPay\Interfaces\PaymentMethodInterface;
use LiliPay\Interfaces\PlanInterface;
use LiliPay\Interfaces\UserInterface;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return [];
    }

    public function initSubscriptionData()
    {
        $address = new Address();
        $user = new User(1, 'Test User PagarmeGateway', 'pagarme@lilipay.com.br', $address, '77839835023', '84999999999');
        $items = [new Item(1, 'Product 1', 1000, 1)];

        app()->bind(UserInterface::class, function () use ($user) {
            return $user;
        });
        app()->bind(AddressInterface::class, function () use ($address) {
            return $address;
        });
        app()->bind(SaleInterface::class, function ($app) use ($user, $items) {
            return new Sale(1234, 1000, $items, 'waiting_payment', $user, 1);
        });
        app()->bind(CreditCardInterface::class, function ($app) {
            return new CreditCard(
                'Test User Pagamarme',
                '5555555555554444',
                '123',
                new ExpirationDate(date('Y') + 1, 10)
            );
        });
        app()->bind(PlanInterface::class, function ($app) {
            return new Plan(1, 'Plan Test', 1000, 30, 1, 0);
        });
        app()->bind(PaymentMethodInterface::class, function ($app) {
            return new PaymentMethod();
        });
    }
}
