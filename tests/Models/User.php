<?php

namespace LiliPay\Tests\Models;

use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\UserInterface;

class User implements UserInterface
{
    private $id;
    private $name;
    private $email;
    private $address;
    private $document;
    private $phone;

    public function __construct($id, $name, $email, AddressInterface $address, $document, $phone)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->address = $address;
        $this->document = $document;
        $this->phone = $phone;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getAddress(): ?AddressInterface
    {
        return $this->address;
    }

    public function getDocument()
    {
        return $this->document;
    }

    public function getPhone()
    {
        return $this->phone;
    }
}
