<?php

namespace LiliPay\Tests\Models;

use LiliPay\Interfaces\RecipientInterface;

class Recipient implements RecipientInterface
{
    private $id;
    private $amount;

    public function __construct($id, $amount)
    {
        $this->id = $id;
        $this->amount = $amount;
    }

    // This test is related to QualificaEAD Account
    public function getRecipientId()
    {
        return $this->id;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
