<?php

namespace LiliPay\Tests\Models;

use LiliPay\Interfaces\ItemInterface;

class Item implements ItemInterface
{

    private $id;
    private $name;
    private $value;
    private $amount;

    public function __construct($id, $name, $value, $amount)
    {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->amount = $amount;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getAmount()
    {
        return $this->amount;
    }
}
