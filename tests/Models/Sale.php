<?php

namespace LiliPay\Tests\Models;

use LiliPay\Interfaces\SaleInterface;
use LiliPay\Interfaces\UserInterface;

class Sale implements SaleInterface
{
    private $transactionCode;
    private $value;
    private $items;
    private $user;
    private $status;
    private $installments;

    public function __construct($code, $value, array $items, $status, UserInterface $user, $installments)
    {
        $this->transactionCode = $code;
        $this->value = $value;
        $this->items = $items;
        $this->user = $user;
        $this->installments = $installments;
        $this->status = $status;
    }

    public function getTransactionCode()
    {
        return $this->transactionCode;
    }

    public function setTransactionCode($code)
    {
        $this->transactionCode = $code;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function payAction($code)
    {
        $this->status = 'paid';
    }

    public function cancelAction($code)
    {
        $this->status = 'cancelled';
    }

    public function getItems()
    {
        return $this->items;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public static function generateTransactionCode()
    {
        return rand(1, 9999);
    }

    public function getDiscount()
    {
        return null;
    }

    public function getInstallments(): int
    {
        return $this->installments;
    }

    public function getInstallmentsValue()
    {
        return $this->value / $this->installments;
    }

    public function getShippingValue(): int
    {
        return 0;
    }
}
