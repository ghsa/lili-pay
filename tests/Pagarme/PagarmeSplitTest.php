<?php

namespace LiliPay\Tests\Pagarme;

use LiliPay\Gateways\Pagarme\Models\PagarmeRecipientList;
use LiliPay\Gateways\Pagarme\PagarmeGateway;
use LiliPay\Gateways\Pagarme\PagarmeSubscriptionGateway;
use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\CreditCardInterface;
use LiliPay\Interfaces\GatewayInterface;
use LiliPay\Interfaces\PaymentMethodInterface;
use LiliPay\Interfaces\PlanInterface;
use LiliPay\Interfaces\SaleInterface;
use LiliPay\Interfaces\SubscriptionGatewayInterface;
use LiliPay\Interfaces\SubscriptionTransactionResponseInterface;
use LiliPay\Interfaces\TransactionResponseInterface;
use LiliPay\Interfaces\UserInterface;
use LiliPay\Models\RecipientList;
use LiliPay\Tests\Models\Recipient;
use LiliPay\Tests\TestCase;

class PagarmeSplitTest extends TestCase
{

    const API_KEY = 'ak_test_yd0r9JkF8upmMY3AChV0Arfp3YJyXZ';
    private $gateway;

    protected function setUp(): void
    {
        app()->bind(SubscriptionGatewayInterface::class, PagarmeSubscriptionGateway::class);
        app()->bind(GatewayInterface::class, PagarmeGateway::class);
        $this->gateway = app(SubscriptionGatewayInterface::class);
        $this->gateway->init(self::API_KEY);

        app()->bind(RecipientList::class, function () {
            return new PagarmeRecipientList();
        });

        $this->initSubscriptionData();
    }

    /** @test */
    public function return_valid_split_recipient_list()
    {
        $recipientList = app(RecipientList::class);
        $recipient = new Recipient('re_cka6q97k102kfu16dkucbyqn8', 80);
        $recipientList->addRecipient($recipient);
        $this->assertTrue(is_array($recipientList->getList()));
    }

    /** @test */
    public function create_a_subscription_with_split()
    {
        // Create plan
        $modelPlan = app(PlanInterface::class);
        $plan = $this->gateway->createPlan($modelPlan);
        $this->assertTrue($plan instanceof PlanInterface);
        $gatewayPlan = $this->gateway->getPlan($plan->getCode());
        $this->assertEquals($gatewayPlan->getCode(), $plan->getCode());

        // Cria um objeto do tipo recipient
        $recipientList = app(RecipientList::class);
        $mainRecipient = new Recipient('re_cka6q97k102kfu16dkucbyqn8', $plan->getValue() * 0.8);
        $secondRecipient = new Recipient('re_cka6q97j402tiex6exz18nsur', $plan->getValue() * 0.2);
        $recipientList->addRecipient($mainRecipient);
        $recipientList->addRecipient($secondRecipient);

        // Create subscription with credit card
        $user = app(UserInterface::class);
        $address = app(AddressInterface::class);
        $crediCard = app(CreditCardInterface::class);
        $paymentMethod = app(PaymentMethodInterface::class);
        $paymentMethod->setAsCreditCard();
        $subscription = $this->gateway->createSubscription($user, $plan, $address, $paymentMethod, $crediCard, $recipientList);
        $this->assertTrue($subscription instanceof SubscriptionTransactionResponseInterface);
        $this->assertEquals('paid', ($subscription->getTransaction())->getPaymentStatus());
    }

    /** @test */
    public function create_billet_payment_with_split()
    {
        // Cria um objeto do tipo recipient
        $sale = app(SaleInterface::class);
        $recipientList = app(RecipientList::class);
        $mainRecipient = new Recipient('re_cka6q97k102kfu16dkucbyqn8', $sale->getValue() * 0.8 * 100);
        $secondRecipient = new Recipient('re_cka6q97j402tiex6exz18nsur', $sale->getValue() * 0.2 * 100);
        $recipientList->addRecipient($mainRecipient);
        $recipientList->addRecipient($secondRecipient);

        $this->gateway = app(GatewayInterface::class);
        $this->gateway->init(self::API_KEY);
        $address = app(AddressInterface::class);
        $response = $this->gateway->sendBilletTransaction($sale,  $address, $recipientList);
        $this->assertTrue($response instanceof TransactionResponseInterface);
        $this->assertTrue($response->getStatus());
        $this->assertEquals('waiting_payment', $response->getPaymentStatus());

        $transaction = $this->gateway->getTransaction($response->getCode());
        $this->assertTrue($transaction instanceof TransactionResponseInterface);
    }

    /** @test */
    public function create_credit_card_payment_with_split()
    {
        // Cria um objeto do tipo recipient
        $recipientList = app(RecipientList::class);
        $sale = app(SaleInterface::class);
        $mainRecipient = new Recipient('re_cka6q97k102kfu16dkucbyqn8', $sale->getValue() * 0.8 * 100);
        $secondRecipient = new Recipient('re_cka6q97j402tiex6exz18nsur', $sale->getValue() * 0.2 * 100);
        $recipientList->addRecipient($mainRecipient);
        $recipientList->addRecipient($secondRecipient);

        $this->gateway = app(GatewayInterface::class);
        $this->gateway->init(self::API_KEY);
        $address = app(AddressInterface::class);
        $creditCard = app(CreditCardInterface::class);
        $response = $this->gateway->sendCreditCardTransaction($sale,  $creditCard, $address, $recipientList);
        $this->assertTrue($response instanceof TransactionResponseInterface);
        $this->assertTrue($response->getStatus());
        $this->assertEquals('paid', $response->getPaymentStatus());

        $transaction = $this->gateway->getTransaction($response->getCode());
        $this->assertTrue($transaction instanceof TransactionResponseInterface);
    }
}
