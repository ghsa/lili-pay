<?php

namespace LiliPay\Tests\Pagarme;

use LiliPay\Gateways\Pagarme\PagarmeWebhook;
use LiliPay\Interfaces\WebhookInterface;
use LiliPay\Tests\TestCase;

class PagarmeWebhookTest extends TestCase
{


    protected function setUp(): void
    {
        app()->bind(WebhookInterface::class, function ($app) {
            return new PagarmeWebhook();
        });
    }

    /** @test */
    public function receive_a_request_and_validate_webhook_methods()
    {
        $request = json_decode($this->getPagarmeWebhookJson(), true);
        $webhook = app(WebhookInterface::class);
        $webhook->initFromRequest($request);
        $this->assertTrue($webhook->isPaid());
        $this->assertEquals(525322, $webhook->getCode());
    }

    private function getPagarmeWebhookJson()
    {
        return '{"id":"525322","fingerprint":"c2392108f30176a041ad81a216e6f80109c15685","event":"subscription_status_changed","old_status":"paid","desired_status":"paid","current_status":"paid","object":"subscription","subscription":{"object":"subscription","plan":{"object":"plan","id":"504088","amount":"10000","days":"365","name":"Plano Teste Pagame2","trial_days":"7","date_created":"2020-09-23T18:25:36.062Z","payment_methods":["boleto","credit_card"],"color":null,"charges":null,"installments":"12","invoice_reminder":null,"payment_deadline_charges_interval":"1"},"id":"525322","current_transaction":{"object":"transaction","status":"waiting_payment","refuse_reason":null,"status_reason":"acquirer","acquirer_response_code":null,"acquirer_name":"pagarme","acquirer_id":"5ebd2eb76c9e533f06d98ada","authorization_code":null,"soft_descriptor":null,"tid":"9936190","nsu":"9936190","date_created":"2020-10-03T14:41:51.966Z","date_updated":"2020-10-03T14:41:52.210Z","amount":"10000","authorized_amount":"10000","paid_amount":"0","refunded_amount":"0","installments":"1","id":"9936190","cost":"0","card_holder_name":null,"card_last_digits":null,"card_first_digits":null,"card_brand":null,"card_pin_mode":null,"card_magstripe_fallback":"false","cvm_pin":null,"postback_url":null,"payment_method":"boleto","capture_method":"ecommerce","antifraud_score":null,"boleto_url":"https:\/\/api.pagar.me\/1\/boletos\/test_ckftsbpwp0n0u0hm5my8r2l56","boleto_barcode":"23791.22928 60000.299846 51000.046907 6 91340000010000","boleto_expiration_date":"2022-10-10T14:41:51.953Z","referer":null,"ip":null,"subscription_id":"525322","metadata":{"foo":"bar"},"reference_key":null,"device":null,"local_transaction_id":null,"local_time":null,"fraud_covered":"false","fraud_reimbursed":null,"order_id":null,"risk_level":"unknown","receipt_url":null,"payment":null,"addition":null,"discount":null,"private_label":null},"postback_url":"http:\/\/localhost.ghsa.ultrahook.com\/webhookSubscription","payment_method":"boleto","card_brand":null,"card_last_digits":null,"current_period_start":"2020-10-03T14:41:51.949Z","current_period_end":"2022-10-10T14:41:51.953Z","charges":"2","soft_descriptor":null,"status":"paid","date_created":"2020-10-03T14:36:05.480Z","date_updated":"2020-10-03T14:41:52.283Z","phone":null,"address":{"object":"address","street":"Rua Murilo Melo","complementary":null,"street_number":"123","neighborhood":"Lagoa Nova","city":"Natal","state":"RN","zipcode":"59054510","country":"Brasil","id":"3463330"},"customer":{"object":"customer","id":"3843552","external_id":null,"type":null,"country":null,"document_number":"07792766470","document_type":"cpf","name":"Gustavo Andrade","email":"ghsa16@gmail.com","phone_numbers":null,"born_at":null,"birthday":null,"gender":null,"date_created":"2020-10-03T14:36:05.148Z"},"card":null,"metadata":{"foo":"bar"},"settled_charges":null,"manage_token":"test_subscription_TH2bAJ3EiLrXH7UhcQdpypL5jNqz7S","manage_url":"https:\/\/pagar.me\/customers\/#\/subscriptions\/525322?token=test_subscription_TH2bAJ3EiLrXH7UhcQdpypL5jNqz7S"}}';
    }
}
