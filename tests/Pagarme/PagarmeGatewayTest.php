<?php

namespace LiliPay\Tests\Pagarme;

use LiliPay\Gateways\Pagarme\PagarmeGateway;
use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\CreditCardInterface;
use LiliPay\Interfaces\GatewayInterface;
use LiliPay\Interfaces\SaleInterface;
use LiliPay\Interfaces\TransactionResponseInterface;
use LiliPay\Interfaces\UserInterface;
use LiliPay\Models\CreditCard;
use LiliPay\Models\ExpirationDate;
use LiliPay\Tests\Models\Sale;
use LiliPay\Tests\Models\User;
use LiliPay\Tests\Models\Address;
use LiliPay\Tests\Models\Item;
use LiliPay\Tests\TestCase;

class PagarmeGatewayTest extends TestCase
{

    const API_KEY = 'ak_test_yd0r9JkF8upmMY3AChV0Arfp3YJyXZ';
    private $gateway;

    protected function setUp(): void
    {
        app()->bind(GatewayInterface::class, PagarmeGateway::class);
        $this->gateway = app(GatewayInterface::class);
        $this->gateway->init(self::API_KEY);

        $address = new Address();
        $user = new User(1, 'Test User PagarmeGateway', 'pagarme@lilipay.com.br', $address, '77839835023', '84999999999');
        $items = [new Item(1, 'Product 1', 1000, 1)];

        app()->bind(UserInterface::class, function () use ($user) {
            return $user;
        });
        app()->bind(AddressInterface::class, function () use ($address) {
            return $address;
        });
        app()->bind(SaleInterface::class, function ($app) use ($user, $items) {
            return new Sale(1234, 1000, $items, 'waiting_payment', $user, 1);
        });
        app()->bind(CreditCardInterface::class, function ($app) {
            return new CreditCard(
                'Test User Pagamarme',
                '5555555555554444',
                '123',
                new ExpirationDate(date('Y') + 1, 10)
            );
        });
    }

    /** @test */
    public function create_a_billet_transaction_and_get_this_same_transaction_again()
    {
        $sale = app(SaleInterface::class);
        $address = app(AddressInterface::class);
        $response = $this->gateway->sendBilletTransaction($sale,  $address);
        $this->assertTrue($response instanceof TransactionResponseInterface);
        $this->assertTrue($response->getStatus());
        $this->assertEquals('waiting_payment', $response->getPaymentStatus());

        $transaction = $this->gateway->getTransaction($response->getCode());
        $this->assertTrue($transaction instanceof TransactionResponseInterface);
    }

    /** @test */
    public function create_a_pix_transaction_and_get_this_same_transaction_again()
    {
        $sale = app(SaleInterface::class);
        $address = app(AddressInterface::class);
        $response = $this->gateway->sendPixTransaction($sale,  $address);
        $this->assertTrue($response instanceof TransactionResponseInterface);
        $this->assertTrue($response->getStatus());
        $this->assertEquals('waiting_payment', $response->getPaymentStatus());

        $transaction = $this->gateway->getTransaction($response->getCode());
        $this->assertTrue($transaction instanceof TransactionResponseInterface);
    }

    /** @test */
    public function create_a_credit_card_transaction_and_get_this_same_transaction_again()
    {
        $sale = app(SaleInterface::class);
        $address = app(AddressInterface::class);
        $creditCard = app(CreditCardInterface::class);
        $response = $this->gateway->sendCreditCardTransaction($sale, $creditCard,  $address);
        $this->assertTrue($response instanceof TransactionResponseInterface);
        $this->assertTrue($response->getStatus());
        $this->assertEquals('paid', $response->getPaymentStatus());

        $transaction = $this->gateway->getTransaction($response->getCode());
        $this->assertTrue($transaction instanceof TransactionResponseInterface);
    }

//    /** @test */
//    public function create_a_credit_card_transaction_and_get_refund_it() {
//        $sale = app(SaleInterface::class);
//        $address = app(AddressInterface::class);
//        $creditCard = app(CreditCardInterface::class);
//        $response = $this->gateway->sendCreditCardTransaction($sale, $creditCard,  $address);
//        $this->assertTrue($response instanceof TransactionResponseInterface);
//        $this->assertTrue($response->getStatus());
//        $this->assertEquals('paid', $response->getPaymentStatus());
//
//        $transaction = $this->gateway->getTransaction($response->getCode());
//        $this->assertTrue($transaction instanceof TransactionResponseInterface);
//
//        $transactionCode = $response->getCode();
//        $this->gateway->sendRefundTransaction($transactionCode);
//    }

}
