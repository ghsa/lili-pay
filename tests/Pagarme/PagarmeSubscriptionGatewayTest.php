<?php

namespace LiliPay\Tests\Pagarme;

use Carbon\Carbon;
use LiliPay\Gateways\Pagarme\PagarmeSubscriptionGateway;
use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\CreditCardInterface;
use LiliPay\Interfaces\PaymentMethodInterface;
use LiliPay\Interfaces\PlanInterface;
use LiliPay\Interfaces\SubscriptionGatewayInterface;
use LiliPay\Interfaces\SubscriptionInterface;
use LiliPay\Interfaces\SubscriptionTransactionResponseInterface;
use LiliPay\Interfaces\UserInterface;

use LiliPay\Tests\TestCase;

class PagarmeSubscriptionGatewayTest extends TestCase
{

    const API_KEY = 'ak_test_yd0r9JkF8upmMY3AChV0Arfp3YJyXZ';
    private $gateway;

    protected function setUp(): void
    {
        app()->bind(SubscriptionGatewayInterface::class, PagarmeSubscriptionGateway::class);
        $this->gateway = app(SubscriptionGatewayInterface::class);
        $this->gateway->init(self::API_KEY);
        $this->initSubscriptionData();
    }

    /** @test */
    public function create_a_plan_and_a_subscription_with_credit_card_and_billet()
    {
        // Create plan
        $modelPlan = app(PlanInterface::class);
        $plan = $this->gateway->createPlan($modelPlan);
        $this->assertTrue($plan instanceof PlanInterface);
        $gatewayPlan = $this->gateway->getPlan($plan->getCode());
        $this->assertEquals($gatewayPlan->getCode(), $plan->getCode());

        // Create subscription with credit card
        $user = app(UserInterface::class);
        $address = app(AddressInterface::class);
        $crediCard = app(CreditCardInterface::class);
        $paymentMethod = app(PaymentMethodInterface::class);
        $paymentMethod->setAsCreditCard();
        $subscription = $this->gateway->createSubscription($user, $plan, $address, $paymentMethod, $crediCard);
        $this->assertTrue($subscription instanceof SubscriptionTransactionResponseInterface);
        $this->assertEquals('paid', ($subscription->getTransaction())->getPaymentStatus());
        $this->assertEquals('paid', ($subscription->getSubscription())->getStatus());
        $this->assertTrue($subscription->getSubscription()->getEndsAt() instanceof Carbon);

        // Create subscription with billet
        $paymentMethod->setAsBillet();
        $billetSubscription = $this->gateway->createSubscription($user, $plan, $address, $paymentMethod);

        $this->assertEquals('waiting_payment', ($billetSubscription->getTransaction())->getPaymentStatus());
        $this->assertEquals('waiting_payment', ($billetSubscription->getSubscription())->getStatus());

        $subscriptionFromGateway = $this->gateway->getSubscription($billetSubscription->getSubscription()->getId());
        $this->assertTrue($subscriptionFromGateway instanceof SubscriptionInterface);
    }
}
