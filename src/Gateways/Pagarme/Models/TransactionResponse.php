<?php

namespace LiliPay\Gateways\Pagarme\Models;

use LiliPay\DTO\PaymentStatusDTO;
use LiliPay\Gateways\Pagarme\DTO\PaymentStatus;
use LiliPay\Interfaces\TransactionResponseInterface;

class TransactionResponse implements TransactionResponseInterface
{

    public $status;
    public $transaction;
    public $message;

    public function __construct(bool $status, string $message, $transaction)
    {
        $this->status = $status;
        $this->message = $message;
        $this->transaction = $transaction;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getCode(): ?string
    {
        if (empty($this->transaction)) {
            return null;
        }
        return $this->transaction->id;
    }

    /**
     * paid, refused, trialing, waiting_payment
     */
    public function getPaymentStatus(): PaymentStatusDTO
    {
        $paymentStatus = new PaymentStatus('none');
        if (!empty($this->transaction)) {
            $paymentStatus->setPaymentStatus($this->transaction->status);
        }
        return $paymentStatus;
    }

    /**
     * Additiona information, like billet url
     */
    public function getPayload(): ?string
    {
        if (empty($this->transaction)) {
            return null;
        }
        if (!empty($this->transaction->current_transaction) && !empty($this->transaction->current_transaction->boleto_url)) {
            return $this->transaction->current_transaction->boleto_url;
        }
        if ($this->transaction->payment_method == 'boleto') {
            return $this->transaction->boleto_url;
        }
        if ($this->transaction->payment_method == 'pix') {
            return $this->transaction->pix_qr_code;
        }
        return null;
    }
}
