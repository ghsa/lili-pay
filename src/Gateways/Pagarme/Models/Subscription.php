<?php

namespace LiliPay\Gateways\Pagarme\Models;

use Carbon\Carbon;
use LiliPay\DTO\PaymentStatusDTO as DTOPaymentStatusDTO;
use LiliPay\Gateways\Pagarme\DTO\PaymentStatus;
use LiliPay\Gateways\Pagarme\DTO\PaymentStatusDTO;
use LiliPay\Interfaces\PaymentMethodInterface;
use LiliPay\Interfaces\PlanInterface;
use LiliPay\Interfaces\SubscriptionInterface;

class Subscription implements SubscriptionInterface
{

    public $id;
    public $value;
    public $status;
    public $endsAt;
    public $paymentMethod;
    public $code;
    public $plan;

    public function __construct($id, $value, $status, PlanInterface $plan, PaymentMethod $paymentMethod, $endsAt = null)
    {
        $this->id = $id;
        $paymentStatus = new PaymentStatus($status);
        $this->status = $paymentStatus;
        $this->paymentMethod = $paymentMethod;
        $this->endsAt = $endsAt;
        $this->value = $value;
        $this->plan = $plan;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGatewayId()
    {
        return $this->code;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getPaymentMethod(): PaymentMethodInterface
    {
        return $this->paymentMethod;
    }

    public function getEndsAt(): ?Carbon
    {
        if (!empty($this->endsAt)) {
            return Carbon::createFromFormat('Y-m-d', substr($this->endsAt, 0, 10));
        }
        return null;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getPlan(): PlanInterface
    {
        return $this->plan;
    }
}
