<?php

namespace LiliPay\Gateways\Pagarme\Models;

use LiliPay\Interfaces\SubscriptionInterface;
use LiliPay\Interfaces\SubscriptionTransactionResponseInterface;
use LiliPay\Interfaces\TransactionResponseInterface;

class SubscriptionTransaction implements SubscriptionTransactionResponseInterface
{
    private $transaction;
    private $subscription;

    public function __construct(TransactionResponseInterface $transaction, $subscription = null)
    {
        $this->transaction = $transaction;
        $this->subscription = $subscription;
    }

    public function getSubscription(): ?SubscriptionInterface
    {
        return $this->subscription;
    }

    public function getTransaction(): TransactionResponseInterface
    {
        return $this->transaction;
    }
}
