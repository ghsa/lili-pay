<?php

namespace LiliPay\Gateways\Pagarme\Models;

use LiliPay\Interfaces\PaymentMethodInterface;

class PaymentMethod implements PaymentMethodInterface
{

    private $paymentMethod;

    public function getCreditCard(): string
    {
        return 'credit_card';
    }

    public function getBillet(): string
    {
        return 'boleto';
    }

    public function getPix(): string
    {
        return 'pix';
    }

    public function setAsCreditCard()
    {
        $this->paymentMethod = $this->getCreditCard();
    }

    public function setAsBillet()
    {
        $this->paymentMethod = $this->getBillet();
    }

    public function setAsPix()
    {
        $this->paymentMethod = $this->getPix();
    }

    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }
}
