<?php

namespace LiliPay\Gateways\Pagarme\Models;

use LiliPay\Models\RecipientList;

class PagarmeRecipientList extends RecipientList
{

    public function getSplitList(): array
    {
        $return = [];

        foreach ($this->list as $recipient) {
            $return[] = [
                'recipient_id' => $recipient->getRecipientId(),
                'amount' => $recipient->getAmount(),
                'liable' => true,
                'charge_processing_fee' => true
            ];
        }

        return $return;
    }
}
