<?php

namespace LiliPay\Gateways\Pagarme;

use LiliPay\Gateways\Pagarme\Adapters\PaymentMethodAdapter;
use LiliPay\Gateways\Pagarme\Adapters\PlanAdapter;
use LiliPay\Gateways\Pagarme\Models\Subscription;
use LiliPay\Gateways\Pagarme\Models\SubscriptionTransaction;
use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\PlanInterface;
use LiliPay\Interfaces\SubscriptionGatewayInterface;
use LiliPay\Interfaces\SubscriptionInterface;
use LiliPay\Interfaces\TransactionResponseInterface;
use LiliPay\Interfaces\UserInterface;
use LiliPay\Gateways\Pagarme\Models\TransactionResponse;
use LiliPay\Interfaces\CreditCardInterface;
use LiliPay\Interfaces\PaymentMethodInterface;
use LiliPay\Interfaces\SubscriptionTransactionResponseInterface;
use LiliPay\Models\Plan;
use LiliPay\Models\RecipientList;
use LiliPay\Traits\Validations;
use PagarMe\Client;

class PagarmeSubscriptionGateway implements SubscriptionGatewayInterface
{
    use Validations;

    private $gateway;
    private $webhookUrl;

    public function init($params)
    {
        $apiKey = (empty($params['api_key'])) ? $params : $params['api_key'];

        $this->gateway = new Client($apiKey);
        if (!empty($params['webhook_url'])) {
            $this->webhookUrl = $params['webhook_url'];
        }
    }

    public function createPlan(PlanInterface $plan): ?PlanInterface
    {
        try {
            $response = $this->gateway->plans()->create([
                'amount' => $plan->getValue(),
                'days' => $plan->getDays(),
                'installments' => $plan->getInstallments(),
                'name' => $plan->getName(),
                'trial_days' => $plan->getTrialDays(),
                'charges' => $plan->getCharges(),
                'payment_methods' => [
                    'boleto',
                    'credit_card'
                ]
            ]);

            $plan->setCode($response->id);
            return $plan;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getPlan($code): ?PlanInterface
    {
        try {
            $res = $this->gateway->plans()->get([
                'id' => $code
            ]);
            return new Plan(
                $res->id,
                $res->name,
                $res->amount,
                $res->days,
                $res->installments,
                $res->trial_days,
                $res->id
            );
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getSubscription($code): ?SubscriptionInterface
    {
        $res = $this->gateway->subscriptions()->get(['id' => $code]);
        if (empty($res)) {
            return null;
        }

        $paymentMethod = $this->getPaymentMethod($res);
        $plan = $this->getPlanAdapter($res->plan);
        $subscription = new Subscription($res->id, 0, $res->status, $plan, $paymentMethod, $res->current_period_end, $res);
        return $subscription;
    }

    public function getBillUrl(SubscriptionInterface $subscription): TransactionResponseInterface
    {
        $res = $this->gateway->subscriptions()->get(['id' => $subscription->getGatewayId()]);
        if (!empty($res->current_transaction->boleto_url)) {
            return new TransactionResponse(true, "Fatura encontrada com sucesso", $res->current_transaction->boleto_url);
        }

        return new TransactionResponse(false, "Error ao encontrar assiantura", null);
    }

    public function createSubscription(
        UserInterface $user,
        PlanInterface $plan,
        AddressInterface $address,
        PaymentMethodInterface $paymentMethod,
        $card = null,
        ?RecipientList $recipientList = null
    ): SubscriptionTransactionResponseInterface
    {
        if (!$this->cpfValidation($user->getDocument())) {
            return new TransactionResponse(false, 'CPF Inválido', null);
        }

        $paymentMethod = $paymentMethod->getPaymentMethod();

        $data = [
            'plan_id' => $plan->getCode(),
            'payment_method' => $paymentMethod,
            'postback_url' => $this->webhookUrl,
            'customer' => $this->getCustomer($user, $address),
            'metadata' => [
                'lib' => 'LiliPay'
            ]
        ];

        if ($paymentMethod == 'credit_card') {
            if (!($card instanceof CreditCardInterface)) {
                throw new \Exception("Credit card should be an instance of CreditCardInterface");
            }

            $expirationDate = $card->getExpirationDate();
            $cardExpDate = $expirationDate->getMonth() . substr($expirationDate->getYear(), -2, 2);
            $data += [
                'card_number' => $card->getNumber(),
                'card_holder_name' => $card->getName(),
                'card_expiration_date' => $cardExpDate,
                'card_cvv' => $card->getCVV(),
            ];
        }

        $data = $this->handlerSplitInformation($data, $recipientList);

        $subscription = null;
        try {
            $responseSubscription = $this->gateway->subscriptions()->create($data);
            $paymentMethod = $this->getPaymentMethod($responseSubscription);
            $plan = $this->getPlanAdapter($responseSubscription->plan);
            $subscription = new Subscription(
                $responseSubscription->id,
                !empty($responseSubscription->current_transaction) ? $responseSubscription->current_transaction->amount : null,
                $responseSubscription->status,
                $plan,
                $paymentMethod,
                $responseSubscription->current_period_end
            );
            $transaction = new TransactionResponse(true, "Assinatur criada com sucesso", $responseSubscription);
        } catch (\Exception $e) {
            $transaction = new TransactionResponse(false, "Preencha todos os campos corretamente", $e);
        }

        return new SubscriptionTransaction($transaction, $subscription);
    }

    private function handlerSplitInformation(array $data, ?RecipientList $recipientList)
    {
        if (empty($recipientList)) {
            return $data;
        }
        $list = $recipientList->getSplitList();
        if (!empty($list)) {
            $data['split_rules'] = $list;
        }

        return $data;
    }

    private function getPlanAdapter($responsePlan): PlanInterface
    {
        $plan = PlanAdapter::getPlan($responsePlan);
        return $plan;
    }

    private function getPaymentMethod($response): PaymentMethodInterface
    {
        return PaymentMethodAdapter::getPaymentMethod($response);
    }

    public function cancelSubscription(SubscriptionInterface $subscription): TransactionResponseInterface
    {
        $res = $this->gateway->subscriptions()->cancel(['id' => $subscription->getGatewayId()]);
        return new TransactionResponse(true, "Assinatura cancelada com sucesso!", $res);
    }

    public function getTransactions(SubscriptionInterface $subscription): TransactionResponseInterface
    {
        $res = $this->gateway->subscriptions()->transactions(['subscription_id' => $subscription->getGatewayId()]);
        return new TransactionResponse(true, "Lista de transações dessa assinatura!", $res);
    }

    private function getCustomer(UserInterface $customer, AddressInterface $address)
    {
        $phoneNumber = $customer->getPhone();
        $ddd = substr($phoneNumber, 0, 2);
        $number = substr($phoneNumber, -9, 9);

        return [
            'email' => $customer->getEmail(),
            'name' => $customer->getName(),
            'document_number' => $customer->getDocument(),
            'phone' => ['ddd' => $ddd, 'number' => $number],
            'address' => [
                'street' => $address->getStreet(),
                'street_number' => $address->getNumber(),
                'complementary' => $address->getComplement(),
                'neighborhood' => $address->getDistrict(),
                'zipcode' => $address->getPostalCode()
            ]
        ];
    }
}
