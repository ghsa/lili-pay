<?php

namespace LiliPay\Gateways\Pagarme\Adapters;

use LiliPay\Interfaces\PlanInterface;
use LiliPay\Models\Plan;

class PlanAdapter
{
    public static function getPlan($responsePlan): PlanInterface
    {
        $plan = new Plan(
            $responsePlan->id,
            $responsePlan->name,
            $responsePlan->amount,
            $responsePlan->days,
            $responsePlan->installments,
            $responsePlan->trial_days,
            $responsePlan->id
        );

        return $plan;
    }
}
