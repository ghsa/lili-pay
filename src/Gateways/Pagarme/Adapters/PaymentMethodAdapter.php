<?php

namespace LiliPay\Gateways\Pagarme\Adapters;

use LiliPay\Gateways\Pagarme\Models\PaymentMethod;
use LiliPay\Interfaces\PaymentMethodInterface;

class PaymentMethodAdapter
{
    public static function getPaymentMethod($response): PaymentMethodInterface
    {
        $paymentMethod = new PaymentMethod();
        if ($response->payment_method == 'credit_card') {
            $paymentMethod->setAsCreditCard();
        } else {
            $paymentMethod->setAsBillet();
        }

        return $paymentMethod;
    }
}
