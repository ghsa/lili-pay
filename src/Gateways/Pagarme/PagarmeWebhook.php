<?php

namespace LiliPay\Gateways\Pagarme;

use LiliPay\Gateways\Pagarme\Adapters\PaymentMethodAdapter;
use LiliPay\Gateways\Pagarme\Adapters\PlanAdapter;
use LiliPay\Gateways\Pagarme\Models\Subscription;
use LiliPay\Interfaces\SubscriptionInterface;
use LiliPay\Interfaces\WebhookInterface;
use LiliPay\Models\WebhookResponse;
use stdClass;

class PagarmeWebhook implements WebhookInterface
{

    private $code;
    private $status;
    private $transaction = null;
    private $subscription = null;

    /**
     * Return a response with status of transaction
     *
     * @param $request
     * @return WebhookResponse
     */
    public function initFromRequest(array $request)
    {
        $this->code = $request['id'];
        if (!empty($request['subscription'])) {
            $this->status = $request['subscription']['status'];
            $this->transaction = $request['subscription']['current_transaction'];
            $subResponse = $request['subscription'];
            $paymentMethod = PaymentMethodAdapter::getPaymentMethod((object) $subResponse);
            $plan = PlanAdapter::getPlan((object) $subResponse['plan']);
            $this->subscription = new Subscription(
                $subResponse['id'],
                0,
                $subResponse['status'],
                $plan,
                $paymentMethod,
                $subResponse['current_period_end'],
                $subResponse
            );
        } else {
            $this->status = !empty($request['current_status']) ? $request['current_status'] : 'waiting';
            $this->transaction = !empty($request['transaction']) ? $request['transaction'] : null;
        }
    }

    public function isPaid(): bool
    {
        if ($this->status == 'paid') {
            return true;
        }
        return false;
    }

    public function isCanceled(): bool
    {
        if ($this->status == 'canceled') {
            return true;
        }
        return false;
    }

    public function isRefunded(): bool
    {
        if ($this->status == 'refunded') {
            return true;
        }
        return false;
    }

    public function isRefused(): bool
    {
        if ($this->status == 'refused') {
            return true;
        }
        return false;
    }

    public function getTransaction()
    {
        return $this->transaction;
    }

    public function getSubscription(): ?SubscriptionInterface
    {
        return $this->subscription;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }
}
