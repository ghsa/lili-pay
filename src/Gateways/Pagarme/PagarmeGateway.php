<?php

namespace LiliPay\Gateways\Pagarme;

use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\GatewayInterface;
use LiliPay\Interfaces\SaleInterface;
use LiliPay\Interfaces\TransactionResponseInterface;
use LiliPay\Gateways\Pagarme\Models\TransactionResponse;
use LiliPay\Helpers\Messages;
use LiliPay\Interfaces\CreditCardInterface;
use LiliPay\Models\RecipientList;
use LiliPay\Traits\Validations;
use PagarMe\Client;

class PagarmeGateway implements GatewayInterface
{

    use Validations;

    private $gateway;
    private $webhookUrl = null;

    public function init($params)
    {
        $apiKey = (empty($params['api_key'])) ? $params : $params['api_key'];
        $this->gateway = new Client($apiKey);

        if (!empty($params['webhook_url'])) {
            $this->webhookUrl = $params['webhook_url'];
        }
        // else if (function_exists('config')) {
        //     $this->webhookUrl = config('services.payment.webhook_url');
        // }
    }

    public function getTransaction($code): ?TransactionResponseInterface
    {
        $transaction = $this->gateway->transactions()->get([
            'id' => $code
        ]);

        return new TransactionResponse(true, Messages::GET_TRANSACTION_SUCCESS, $transaction);
    }

    private function applyShipping(SaleInterface $sale, array $data)
    {
        if ($sale->getShippingValue() > 0) {
            $customer = $sale->getUser();
            $address = $customer->getAddress();
            $data['shipping'] = [
                'name' => $customer->getName(),
                'fee' => $sale->getShippingValue(),
                'address' => [
                    'country' => $address->getCountry(),
                    'street' => $address->getStreet(),
                    'street_number' => $address->getNumber(),
                    'state' => $address->getState(),
                    'city' => $address->getCity(),
                    'neighborhood' => $address->getDistrict(),
                    'zipcode' => $address->getPostalCode()
                ]
            ];
        };

        return $data;
    }

    public function sendPixTransaction(
        SaleInterface $sale,
        AddressInterface $address,
        ?RecipientList $receipientList = null
    ): TransactionResponseInterface {
        $customer = $sale->getUser();
        if (!$this->cpfValidation($customer->getDocument())) {
            return new TransactionResponse(false, 'CPF Inválido', null);
        }
        try {
            $data = [
                'amount' => (int)($sale->getValue() * 100),
                'payment_method' => 'pix',
                'pix_expiration_date' => date('Y-m-d'),
                'pix_additional_fields' => $this->getPixItems($sale),
                'postback_url' => $this->webhookUrl,
                'async' => false,
                'customer' => $this->getCustomer($customer),
                'billing' => $this->getBillingAddress($customer, $address),
                'items' => $this->getItems($sale)
            ];
            $data = $this->applyShipping($sale, $data);
            $data = $this->handlerSplitInformation($data, $receipientList);
            $transaction = $this->gateway->transactions()->create($data);
            $sale->setTransactionCode($transaction->id);
            return new TransactionResponse(true, Messages::BILLET_CREATED, $transaction);
        } catch (\Exception $e) {
            return new TransactionResponse(false, $e->getMessage(), $e);
        }
    }

    public function sendBilletTransaction(
        SaleInterface $sale,
        AddressInterface $address,
        ?RecipientList $receipientList = null
    ): TransactionResponseInterface {
        $customer = $sale->getUser();
        if (!$this->cpfValidation($customer->getDocument())) {
            return new TransactionResponse(false, 'CPF Inválido', null);
        }
        try {
            $data = [
                'amount' => (int)($sale->getValue() * 100),
                'payment_method' => 'boleto',
                'postback_url' => $this->webhookUrl,
                'async' => false,
                'customer' => $this->getCustomer($customer),
                'billing' => $this->getBillingAddress($customer, $address),
                'items' => $this->getItems($sale)
            ];
            $data = $this->applyShipping($sale, $data);
            $data = $this->handlerSplitInformation($data, $receipientList);
            $transaction = $this->gateway->transactions()->create($data);
            $sale->setTransactionCode($transaction->id);
            return new TransactionResponse(true, Messages::BILLET_CREATED, $transaction);
        } catch (\Exception $e) {
            return new TransactionResponse(false, $e->getMessage(), $e);
        }
    }

    public function sendCreditCardTransaction(
        SaleInterface $sale,
        CreditCardInterface $card,
        AddressInterface $address,
        ?RecipientList $receipientList = null
    ): TransactionResponseInterface {
        $customer = $sale->getUser();

        if (!$this->cpfValidation($customer->getDocument())) {
            return new TransactionResponse(false, Messages::INVALID_DOCUMENT, null);
        }

        try {
            $expirationDate = $card->getExpirationDate();
            $cardExpDate = $expirationDate->getMonth() . substr($expirationDate->getYear(), -2, 2);

            $data = [
                'amount' => (int) ($sale->getValue() * 100),
                'payment_method' => 'credit_card',
                'postback_url' => $this->webhookUrl,
                'async' => false,
                'installments' => $sale->getInstallments(),
                'card_holder_name' => $card->getName(),
                'card_cvv' => $card->getCVV(),
                'card_number' => $card->getNumber(),
                'card_expiration_date' => $cardExpDate,
                'customer' => $this->getCustomer($customer),
                'billing' => $this->getBillingAddress($customer, $address),
                'items' => $this->getItems($sale)
            ];
            $data = $this->applyShipping($sale, $data);
            $data = $this->handlerSplitInformation($data, $receipientList);

            $transaction = $this->gateway->transactions()->create($data);
            $sale->setTransactionCode($transaction->id);

            if ($transaction->status == 'paid') {
                $sale->payAction($transaction->id);
                return new TransactionResponse(true, Messages::CREDIT_CARD_CREATED_AND_PAID, $transaction);
            } elseif ($transaction->status == 'rejected') {
                $sale->cancelAction($transaction->id);
                return new TransactionResponse(true, Messages::CREDIT_CARD_CREATED_BUT_DENIED, $transaction);
            }

            return new TransactionResponse(true, Messages::CREDIT_CARD_CREATED_AND_WAITING_PAYMENT, $transaction);
        } catch (\Exception $e) {
            return new TransactionResponse(false, $e->getMessage(), $e);
        }
    }

    private function handlerSplitInformation(array $data, ?RecipientList $recipientList)
    {
        if (empty($recipientList)) {
            return $data;
        }
        $list = $recipientList->getSplitList();
        if (!empty($list)) {
            $data['split_rules'] = $list;
        }

        return $data;
    }

    private function transformExpirationDate()
    {
    }

    private function getCustomer($customer)
    {
        return [
            'external_id' => $customer->getId() . '',
            'name' => $customer->getName(),
            'type' => 'individual',
            'country' => 'br',
            'documents' => [
                [
                    'type' => 'cpf',
                    'number' => $customer->getDocument()
                ]
            ],
            //'document_number' => $customer->getDocument(),
            'phone_numbers' => ['+55' . $customer->getPhone()],
            'email' => $customer->getEmail()
        ];
    }

    private function getPixItems(SaleInterface $sale)
    {
        $items = [];
        foreach ($sale->getItems() as $item) {
            $items[] = [
                'name' => substr($item->getName(), 0, 40),
                'value' => ($item->getValue() * 100) . ''
            ];
        }
        return $items;
    }

    private function getItems(SaleInterface $sale)
    {
        $items = [];
        foreach ($sale->getItems() as $item) {
            $items[] = [
                'id' => $item->getId() . '',
                'title' => $item->getName(),
                'unit_price' => (int) ($item->getValue() * 100),
                'quantity' => $item->getAmount(),
                'tangible' => false,
                'date' => date('Y-m-d'),

            ];
        }
        return $items;
    }

    private function getBillingAddress($customer, AddressInterface $address)
    {
        return [
            'name' => $customer->getName(),
            'address' => [
                'country' => $address->getCountry(),
                'street' => $address->getStreet(),
                'street_number' => $address->getNumber(),
                'state' => $address->getState(),
                'city' => $address->getCity(),
                'neighborhood' => $address->getDistrict(),
                'zipcode' => $address->getPostalCode()
            ]
        ];
    }
}
