<?php

namespace LiliPay\Gateways\Pagarme\DTO;

use LiliPay\DTO\PaymentStatusDTO;

class PaymentStatus extends PaymentStatusDTO
{
    public function __construct($status)
    {
        $this->setPaymentStatus($status);
    }

    public function setPaymentStatus(string $paymentStatus)
    {
        switch ($paymentStatus) {
            case 'paid':
                $this->paymentStatus = PaymentStatusDTO::PAID;
                break;
            case 'waiting_payment':
                $this->paymentStatus = PaymentStatusDTO::WAITING_PAYMENT;
                break;
            case 'cancelled':
            case 'refused':
                $this->paymentStatus = PaymentStatusDTO::CANCELLED;
                break;
            case 'unpaid':
                $this->paymentStatus = PaymentStatusDTO::WAITING_PAYMENT;
                break;
            case 'trialing':
                $this->paymentStatus = PaymentStatusDTO::TRIAL;
                break;
            default:
                $this->paymentStatus = PaymentStatusDTO::NONE;
        }
    }
}
