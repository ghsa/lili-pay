<?php

namespace LiliPay\Gateways;

use LiliPay\Interfaces\AddressInterface;
use LiliPay\Interfaces\CreditCardInterface;
use LiliPay\Interfaces\GatewayInterface;
use LiliPay\Interfaces\SaleInterface;

class PagSeguroGateway implements GatewayInterface
{

    public function init()
    {
        dd('test');
    }

    public function sendCreditCardTransaction(
        SaleInterface $sale,
        CreditCardInterface $creditCard,
        AddressInterface $billingAddress,
        AddressInterface $shippingAddress
    ): SaleInterface {
        $transactionCode = $sale->generateTransactionCode();

        $user = $sale->getUser();

        $itens = [];
        foreach ($sale->getItems() as $i) {
            $itens[] = [
                'itemId' => $i->getId(),
                'itemDescription' => $i->getName(),
                'itemAmount' => $i->getValue(),
                'itemQuantity' => $i->getAmount(),
            ];
        }

        $payment = \PagSeguro::setReference($transactionCode)
            ->setSenderInfo([
                'senderName' => $user->getName(), //Deve conter nome e sobrenome
                'senderPhone' => $user->getPhone(), //Código de área enviado junto com o telefone
                'senderEmail' => $user->getEmail(),
                'senderHash' => $user->getId(),
                'senderCPF' => $user->getDocument() //Ou CNPJ se for Pessoa Júridica
            ])
            ->setCreditCardHolder([
                'creditCardHolderName' => $creditCard->getName(), //Deve conter nome e sobrenome
                'creditCardHolderPhone' => $creditCard->getPhone(), //Código de área enviado junto com o telefone
                'creditCardHolderCPF' => $creditCard->getDocument(), //Ou CNPJ se for Pessoa Júridica
                'creditCardHolderBirthDate' => $creditCard->getBirthDate(),
            ])
            ->setBillingAddress([
                'billingAddressStreet' => $billingAddress->getStreet(),
                'billingAddressNumber' => $billingAddress->getNumber(),
                'billingAddressDistrict' => $billingAddress->getDistrict(),
                'billingAddressPostalCode' => $billingAddress->getPostalCode(),
                'billingAddressCity' => $billingAddress->getCity(),
                'billingAddressState' => $billingAddress->getState()
            ])
            ->setShippingAddress([
                'shippingAddressStreet' => $shippingAddress->getStreet(),
                'shippingAddressNumber' => $shippingAddress->getNumber(),
                'shippingAddressDistrict' => $shippingAddress->getDistrict(),
                'shippingAddressPostalCode' => $shippingAddress->getPostalCode(),
                'shippingAddressCity' => $shippingAddress->getCity(),
                'shippingAddressState' => $shippingAddress->getState()
            ])
            ->setItems($itens)
            ->setExtraAmount(-$sale->getDiscount())
            ->send([
                'paymentMethod' => 'creditCard',
                'creditCardToken' => $creditCard->getHash(),
                'installmentQuantity' => $sale->getInstallments(),
                'installmentValue' => $sale->getInstallmentsValue()
            ]);

        dd($payment);
        return $sale;
    }


    public function sendBilletTransaction(SaleInterface $sale): SaleInterface
    {
        $transaction = $sale->generateTransactionCode();

        return $sale;
    }
}
