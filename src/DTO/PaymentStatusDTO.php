<?php

namespace LiliPay\DTO;

abstract class PaymentStatusDTO
{
    const NONE = 'none';
    const PAID = 'paid';
    const WAITING_PAYMENT = 'waiting_payment';
    const UNPAID = 'unpaid';
    const CANCELLED = 'cancelled';
    const TRIAL = 'trial';
    const ACTIVE = 1;
    const INT_TRIAL = 2;
    const INACTIVE = 3;

    protected $paymentStatus = null;

    abstract public function setPaymentStatus(string $paymentStatus);

    public function getPaymentStatus(): string
    {
        switch ($this->paymentStatus) {
            case self::PAID:
                return self::PAID;
            case self::WAITING_PAYMENT:
                return self::WAITING_PAYMENT;
            case self::UNPAID:
                return self::UNPAID;
            case self::CANCELLED:
                return self::CANCELLED;
            case self::TRIAL:
                return self::TRIAL;
            default:
                return self::NONE;
        }
    }

    public function __toString()
    {
        return $this->getPaymentStatus();
    }
}
