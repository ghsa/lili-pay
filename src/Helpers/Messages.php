<?php

namespace LiliPay\Helpers;

class Messages
{
    const GET_TRANSACTION_SUCCESS = 'Transaction successfully returned.';
    const BILLET_CREATED = 'Billet successfully created.';
    const FILL_COMPLETE_DATA = 'Fill all fields.';
    const CREDIT_CARD_CREATED_AND_PAID = 'Success credit card created and paid.';
    const CREDIT_CARD_CREATED_BUT_DENIED = 'Success credit card created but card denied.';
    const CREDIT_CARD_CREATED_AND_WAITING_PAYMENT = 'Success credit card created but waiting payment';
    const INVALID_DOCUMENT = 'Invalid CPF';
}
