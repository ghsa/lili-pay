<?php

namespace LiliPay\Models;

class WebhookResponse
{

    public $code;
    public $status;
    public $transaction;

    public function __construct($code, $status, $transaction = null)
    {
        $this->status = $status;
        $this->code = $code;
        $this->transaction = $transaction;
    }

    public function getCode()
    {
        return $this->code;
    }
}
