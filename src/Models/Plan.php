<?php

namespace LiliPay\Models;

use LiliPay\Interfaces\PlanInterface;

class Plan implements PlanInterface
{
    private $id;
    private $value;
    private $days;
    private $installments;
    private $name;
    private $trialDays;
    private $code;
    private $charges;

    public function __construct($id, $name, $value, $days, $installments, $trialDays = 0, $code = null, $charges = 1)
    {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->days = $days;
        $this->installments = $installments;
        $this->trialDays = $trialDays;
        $this->code = $code;
        $this->charges = $charges;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getInstallments()
    {
        return $this->installments;
    }

    public function getTrialDays()
    {
        return $this->trialDays;
    }

    public function getDays()
    {
        return $this->days;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getCharges()
    {
        return $this->charges;
    }

}
