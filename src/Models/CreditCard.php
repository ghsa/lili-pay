<?php

namespace LiliPay\Models;

use LiliPay\Interfaces\CreditCardInterface;

class CreditCard implements CreditCardInterface
{

    private $holderName;
    private $number;
    private $cvv;
    private $expirationDate;
    private $hash;
    private $phone;
    private $document;
    private $birthDate;

    public function __construct(
        $holderName,
        $number,
        $cvv,
        $expirationDate,
        $hash = null,
        $birthDate = null,
        $phone = null,
        $document = null
    ) {
        $this->holderName = $holderName;
        $this->number = $number;
        $this->cvv = $cvv;
        $this->expirationDate = $expirationDate;
        $this->hash = $hash;
        $this->phone = $phone;
        $this->document = $document;
        $this->birthDate = $birthDate;
    }

    public function getHolderName()
    {
        return $this->holderName;
    }

    public function getName()
    {
        return $this->holderName;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getCVV()
    {
        return $this->cvv;
    }

    public function getExpirationDate(): ExpirationDate
    {
        return $this->expirationDate;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getDocument(): ?string
    {
        return $this->document;
    }

    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }
}
