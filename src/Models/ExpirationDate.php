<?php

namespace LiliPay\Models;

class ExpirationDate
{
    private $year;
    private $month;
    public function __construct(int $year, int $month)
    {
        $this->year = $year;
        $this->month = $month;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getMonth()
    {
        if ($this->month < 10) {
            return "0" . $this->month;
        }
        return $this->month;
    }
}
