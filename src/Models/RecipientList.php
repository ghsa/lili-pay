<?php

namespace LiliPay\Models;

use LiliPay\Interfaces\RecipientInterface;

abstract class RecipientList
{
    protected $list = [];

    public function addRecipient(RecipientInterface $recipient)
    {
        $this->list[] = $recipient;
    }

    public function getList()
    {
        return $this->list;
    }

    /**
     * Return array list preparet to use in split request
     */
    abstract public function getSplitList(): array;
}
