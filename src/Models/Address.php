<?php

namespace LiliPay\Models;

use LiliPay\Interfaces\AddressInterface;

class Address implements AddressInterface
{

    private $country;
    private $street;
    private $number;
    private $state;
    private $city;
    private $neighborhood;
    private $zipcode;
    private $complement;

    public function __construct($country, $street, $number, $state, $city, $neighborhood, $zipcode, $complement = null)
    {
        $this->country = $country;
        $this->street = $street;
        $this->number = $number;
        $this->state = $state;
        $this->city = $city;
        $this->neighborhood = $neighborhood;
        $this->zipcode = $zipcode;
        $this->complement = $complement;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getZipCode()
    {
        return $this->zipcode;
    }

    public function getPostalCode()
    {
        return $this->zipcode;
    }

    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    public function getDistrict()
    {
        return $this->neighborhood;
    }

    public function getComplement()
    {
        return $this->complement;
    }
}
