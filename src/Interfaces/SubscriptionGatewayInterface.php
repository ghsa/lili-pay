<?php

namespace LiliPay\Interfaces;


interface SubscriptionGatewayInterface
{
    public function init($params);

    public function createPlan(PlanInterface $plan): ?PlanInterface;

    public function getPlan($code): ?PlanInterface;

    public function createSubscription(UserInterface $user, PlanInterface $plan, AddressInterface $address, PaymentMethodInterface $paymentMethod, $card = null): SubscriptionTransactionResponseInterface;

    public function getSubscription($code): ?SubscriptionInterface;

    public function cancelSubscription(SubscriptionInterface $subscription): TransactionResponseInterface;

    public function getTransactions(SubscriptionInterface $subscription): TransactionResponseInterface;
}
