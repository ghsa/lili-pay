<?php

namespace LiliPay\Interfaces;

use Faker\Provider\ar_SA\Payment;
use LiliPay\DTO\PaymentStatusDTO;

interface TransactionResponseInterface
{
    /**
     * Return if the transaction was successfully
     */
    public function getStatus(): bool;

    /**
     * Return transaction content
     */
    public function getTransaction();

    public function getMessage(): ?string;

    public function getCode(): ?string;

    public function getPaymentStatus(): PaymentStatusDTO;

    /**
     * Additiona information, like billet url
     */
    public function getPayload(): ?string;
}
