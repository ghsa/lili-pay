<?php

namespace LiliPay\Interfaces;

use LiliPay\Models\Address;

interface UserInterface
{
    public function getId();

    public function getName();

    public function getEmail();

    public function getAddress(): ?AddressInterface;

    public function getDocument();

    public function getPhone();
}
