<?php

namespace LiliPay\Interfaces;

interface WebhookInterface
{
    public function initFromRequest(array $request);

    public function getCode(): ?string;

    public function getTransaction();

    public function getSubscription(): ?SubscriptionInterface;

    public function isPaid(): bool;

    public function isRefused(): bool;

    public function isCanceled(): bool;
}
