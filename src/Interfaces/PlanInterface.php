<?php

namespace LiliPay\Interfaces;

interface PlanInterface
{
    public function getId();

    public function setCode($code);

    // Plan in at gateway
    public function getCode();

    public function getName();

    public function getValue();

    public function getDays();

    public function getCharges();

    public function getInstallments();

    public function getTrialDays();
}
