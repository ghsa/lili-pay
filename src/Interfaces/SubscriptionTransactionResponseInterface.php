<?php

namespace LiliPay\Interfaces;

interface SubscriptionTransactionResponseInterface
{
    public function getSubscription(): ?SubscriptionInterface;

    public function getTransaction(): TransactionResponseInterface;
}
