<?php

namespace LiliPay\Interfaces;

interface PaymentMethodInterface
{
    public function getCreditCard(): string;

    public function getBillet(): string;

    public function getPix(): string;

    public function setAsCreditCard();

    public function setAsBillet();

    public function setAsPix();

    public function getPaymentMethod(): string;
}
