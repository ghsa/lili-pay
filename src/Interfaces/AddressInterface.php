<?php

namespace LiliPay\Interfaces;

interface AddressInterface
{
    public function getStreet();

    public function getNumber();

    public function getDistrict();

    public function getPostalCode();

    public function getCity();

    public function getState();

    public function getComplement();

    public function getCountry();
}
