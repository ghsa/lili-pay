<?php

namespace LiliPay\Interfaces;

interface SaleInterface
{
    public function getTransactionCode();

    public function setTransactionCode($code);

    public function getValue();

    public function payAction($code);

    public function cancelAction($code);

    public function getItems();

    public function getUser(): UserInterface;

    public function getStatus();

    public static function generateTransactionCode();

    public function getDiscount();

    public function getInstallments(): int;

    public function getInstallmentsValue();

    public function getShippingValue(): int;
}
