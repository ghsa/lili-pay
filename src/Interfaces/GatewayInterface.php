<?php

namespace LiliPay\Interfaces;

use LiliPay\Models\RecipientList;

/**
 * Create simple payment transactions
 */
interface GatewayInterface
{
    public function init($params);

    public function sendBilletTransaction(SaleInterface $sale, AddressInterface $address, ?RecipientList $recipientList): TransactionResponseInterface;

    public function sendCreditCardTransaction(SaleInterface $sale, CreditCardInterface $card, AddressInterface $address, ?RecipientList $recipientList): TransactionResponseInterface;

    public function getTransaction($code): ?TransactionResponseInterface;

//    public function sendRefundTransaction($code);
}
