<?php

namespace LiliPay\Interfaces;

interface ItemInterface
{
    public function getName();

    public function getId();

    public function getValue();

    public function getAmount();
}
