<?php

namespace LiliPay\Interfaces;

use LiliPay\Models\ExpirationDate;

interface CreditCardInterface
{
    public function getName();

    public function getPhone(): ?string;

    public function getDocument(): ?string;

    public function getBirthDate(): ?string;

    public function getHash(): ?string;

    public function getNumber();

    public function getExpirationDate(): ExpirationDate;

    public function getCVV();
}
