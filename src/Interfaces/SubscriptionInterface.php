<?php

namespace LiliPay\Interfaces;

use Carbon\Carbon;
use LiliPay\DTO\PaymentStatusDTO;

/**
 * Create Subscription Transactions
 */
interface SubscriptionInterface
{
    public function getId();

    public function getGatewayId();

    public function getValue();

    public function getPaymentMethod(): PaymentMethodInterface;

    public function getStatus();

    public function getEndsAt(): ?Carbon;

    public function getPlan(): PlanInterface;
}
