<?php

namespace LiliPay\Interfaces;

interface RecipientInterface
{
    public function getRecipientId();

    public function getAmount(): int;
}
