<?php

namespace LiliPay\Interfaces;

use LiliPay\Models\TransactionResponse;

interface BillableSubscriptionGatewayInterface
{
    public function getBillUrl(SubscriptionInterface $subscription): TransactionResponse;
}
